<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Equipe;
use AppBundle\Entity\Etude;
use AppBundle\Entity\Session;
use AppBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\BrowserKit\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use League\Csv\Writer;
/**
 * Session controller.
 *
 * @Route("session")
 */
class SessionController extends Controller
{
    /**
     * Lists all session entities.
     *
     * @Route("/", name="session_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $sessions = $em->getRepository('AppBundle:Session')->findAll();
        return $this->render('session/index.html.twig', array(
            'sessions' => $sessions,
        ));
    }

    /**
     * Create a new CSV with all test
     * @Route("/export/{id}", name="session_export")
     *
     */
    public function exportAction(Request $request,Session $session){
        $em = $this->getDoctrine()->getRepository("AppBundle:Test");
        /*
         * Every test is a row
         */
        $tests = $em->findBy(['session'=>$session]);
        $header= ["Antibiotique","AntibioPlus","Souche","Resultat"];
        $row=array();

        foreach ($tests as $test){


               $row[]=[$test->getAntibiotique()->getNom(), $test->getMoleculeAntibioplus()->getNom(),$test->getSouche()->getNom(),$test->getResultat()];

        }

        $csv = Writer::createFromString("");
        $csv->setDelimiter(";"); //the delimiter will be the tab character
        $csv->setNewline("\r\n"); //use windows line endings for compatibility with some csv libraries
        $csv->setOutputBOM(Writer::BOM_UTF8); //adding the BOM sequence on output
        $csv->insertOne($header);
        $csv->insertAll($row);
        $filename = "../web/exportfile/ExportSession".$session->getId().".csv";
        file_put_contents($filename,$csv);
        $response = new BinaryFileResponse($filename);
        $response->setContentDisposition(ResponseHeaderBag::DISPOSITION_ATTACHMENT,'export.csv');
        return $response;


    }
    /**
     * Creates a new session entity.
     *
     * @Route("/new/{id}", name="session_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request,Etude $id)
    {
        $em = $this->getDoctrine()->getRepository("AppBundle:Etude");

        $etude = $em->find($id);
        $session = new Session();
        $session->setEtude($etude);

        $form = $this->createForm('AppBundle\Form\SessionType', $session);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($session);
            $em->flush($session);

            return $this->redirectToRoute('session_show', array('id' => $session->getId()));
        }

        return $this->render('session/new.html.twig', array(
            'session' => $session,
            'form' => $form->createView(),
            'etude'=>$etude
        ));
    }

    /**
     * Finds and displays a session entity.
     *
     * @Route("/{id}", name="session_show")
     * @Method("GET")
     */
    public function showAction(Session $session)
    {

        $deleteForm = $this->createDeleteForm($session);
        $em = $this->getDoctrine()->getRepository("AppBundle:Test");
        $tests = $em->findBy(['session'=>$session]);
        return $this->render('session/show.html.twig', array(
            'tests' => $tests,
            'session' => $session,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing session entity.
     *
     * @Route("/{id}/edit", name="session_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Session $session)
    {
        if ($this->getUser()->getId() != $session->getEtude()->getAdministrateur()->getId()){
            return $this->redirectToRoute("homepage");
        }
        $deleteForm = $this->createDeleteForm($session);
        $editForm = $this->createForm('AppBundle\Form\SessionType', $session);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('session_show', array('id' => $session->getId()));
        }

        return $this->render('session/edit.html.twig', array(
            'session' => $session,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a session entity.
     *
     * @Route("/{id}", name="session_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Session $session)
    {
        if ($this->getUser()->getId() != $session->getEtude()->getAdministrateur()->getId()){
            return $this->redirectToRoute("homepage");
        }
        $form = $this->createDeleteForm($session);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($session);
            $em->flush($session);
        }

        return $this->redirectToRoute('session_index');
    }

    /**
     * Creates a form to delete a session entity.
     *
     * @param Session $session The session entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Session $session)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('session_delete', array('id' => $session->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }

    private function InTeam(Equipe $equipe)
    {
    foreach ($equipe->getUsers() as $membres){
        if ($membres->getId() == $this->getUser()->getId()){
            return true;
        }
    }
    return false;
    }

}