<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Molecule_AntiBioPlus;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Molecule_antibioplus controller.
 *
 * @Route("molecule_antibioplus")
 */
class Molecule_AntiBioPlusController extends Controller
{
    /**
     * Lists all molecule_AntiBioPlus entities.
     *
     * @Route("/", name="molecule_antibioplus_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $molecule_AntiBioPluses = $em->getRepository('AppBundle:Molecule_AntiBioPlus')->findAll();

        return $this->render('molecule_antibioplus/index.html.twig', array(
            'molecule_AntiBioPluses' => $molecule_AntiBioPluses,
        ));
    }

    /**
     * Creates a new molecule_AntiBioPlus entity.
     *
     * @Route("/new", name="molecule_antibioplus_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $molecule_AntiBioPlus = new Molecule_antibioplus();
        $form = $this->createForm('AppBundle\Form\Molecule_AntiBioPlusType', $molecule_AntiBioPlus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($molecule_AntiBioPlus);
            $em->flush($molecule_AntiBioPlus);

            return $this->redirectToRoute('molecule_antibioplus_show', array('id' => $molecule_AntiBioPlus->getId()));
        }

        return $this->render('molecule_antibioplus/new.html.twig', array(
            'molecule_AntiBioPlus' => $molecule_AntiBioPlus,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a molecule_AntiBioPlus entity.
     *
     * @Route("/{id}", name="molecule_antibioplus_show")
     * @Method("GET")
     */
    public function showAction(Molecule_AntiBioPlus $molecule_AntiBioPlus)
    {
        $deleteForm = $this->createDeleteForm($molecule_AntiBioPlus);

        return $this->render('molecule_antibioplus/show.html.twig', array(
            'molecule_AntiBioPlus' => $molecule_AntiBioPlus,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing molecule_AntiBioPlus entity.
     *
     * @Route("/{id}/edit", name="molecule_antibioplus_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Molecule_AntiBioPlus $molecule_AntiBioPlus)
    {
        $deleteForm = $this->createDeleteForm($molecule_AntiBioPlus);
        $editForm = $this->createForm('AppBundle\Form\Molecule_AntiBioPlusType', $molecule_AntiBioPlus);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('molecule_antibioplus_edit', array('id' => $molecule_AntiBioPlus->getId()));
        }

        return $this->render('molecule_antibioplus/edit.html.twig', array(
            'molecule_AntiBioPlus' => $molecule_AntiBioPlus,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a molecule_AntiBioPlus entity.
     *
     * @Route("/{id}", name="molecule_antibioplus_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Molecule_AntiBioPlus $molecule_AntiBioPlus)
    {
        $form = $this->createDeleteForm($molecule_AntiBioPlus);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($molecule_AntiBioPlus);
            $em->flush($molecule_AntiBioPlus);
        }

        return $this->redirectToRoute('molecule_antibioplus_index');
    }

    /**
     * Creates a form to delete a molecule_AntiBioPlus entity.
     *
     * @param Molecule_AntiBioPlus $molecule_AntiBioPlus The molecule_AntiBioPlus entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Molecule_AntiBioPlus $molecule_AntiBioPlus)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('molecule_antibioplus_delete', array('id' => $molecule_AntiBioPlus->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
