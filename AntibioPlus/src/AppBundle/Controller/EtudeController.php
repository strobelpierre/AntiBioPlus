<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Etude;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\User\User;

/**
 * Etude controller.
 *
 * @Route("etude")
 */
class EtudeController extends Controller
{
    /**
     * Lists all etude entities.
     *
     * @Route("/", name="etude_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $etudes = $em->getRepository('AppBundle:Etude')->findBy(["administrateur"=>$this->getUser()]);


        return $this->render('etude/index.html.twig', array(
            'etudes' => $etudes,
        ));
    }

    /**
     * Creates a new etude entity.
     *
     * @Route("/new", name="etude_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $etude = new Etude();
        $form = $this->createForm('AppBundle\Form\EtudeType', $etude);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($etude);
            $em->flush($etude);

            return $this->redirectToRoute('etude_show', array('id' => $etude->getId()));
        }

        return $this->render('etude/new.html.twig', array(
            'etude' => $etude,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a etude entity.
     *
     * @Route("/{id}", name="etude_show")
     * @Method("GET")
     */
    public function showAction(Etude $etude)
    {


        /**
         * Si l'utilisateur courant n'est pas l'admin de l'étude
         */
        $user = $this->getUser();
        if ($user->getID() != $etude->getAdministrateur()->getId()){
            $this->addFlash("notice","Vous n'avez pas accèes à cette page");
           return $this->redirectToRoute("homepage");
        }
        $deleteForm = $this->createDeleteForm($etude);
        $em = $this->getDoctrine()->getRepository("AppBundle:Session");
        $sessions = $em->findBy(["etude"=>$etude]);
        return $this->render('etude/show.html.twig', array(
            'etude' => $etude,
            'delete_form' => $deleteForm->createView(),
            'sessions' => $sessions

        ));
    }

    /**
     * Displays a form to edit an existing etude entity.
     *
     * @Route("/{id}/edit", name="etude_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Etude $etude)
    {
        $user = $this->getUser();
        if ($user->getID() != $etude->getAdministrateur()->getId()){
            $this->addFlash("Vous n'avez pas accèes à cette page");
            return $this->redirectToRoute("homepage");
        }
        $deleteForm = $this->createDeleteForm($etude);
        $editForm = $this->createForm('AppBundle\Form\EtudeType', $etude);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('etude_edit', array('id' => $etude->getId()));
        }

        return $this->render('etude/edit.html.twig', array(
            'etude' => $etude,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a etude entity.
     *
     * @Route("/{id}", name="etude_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Etude $etude)
    {
        $user = $this->getUser();
        if ($user->getID() != $etude->getAdministrateur()->getId()){
            $this->addFlash("Vous n'avez pas accèes à cette page");
            return $this->redirectToRoute("homepage");
        }
        $form = $this->createDeleteForm($etude);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($etude);
            $em->flush($etude);
        }

        return $this->redirectToRoute('etude_index');
    }

    /**
     * Creates a form to delete a etude entity.
     *
     * @param Etude $etude The etude entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Etude $etude)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('etude_delete', array('id' => $etude->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
