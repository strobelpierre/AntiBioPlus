<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Antibiotique;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Antibiotique controller.
 *
 * @Route("antibiotique")
 */
class AntibiotiqueController extends Controller
{
    /**
     * Lists all antibiotique entities.
     *
     * @Route("/", name="antibiotique_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $antibiotiques = $em->getRepository('AppBundle:Antibiotique')->findAll();

        return $this->render('antibiotique/index.html.twig', array(
            'antibiotiques' => $antibiotiques,
        ));
    }

    /**
     * Creates a new antibiotique entity.
     *
     * @Route("/new", name="antibiotique_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $antibiotique = new Antibiotique();
        $form = $this->createForm('AppBundle\Form\AntibiotiqueType', $antibiotique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($antibiotique);
            $em->flush($antibiotique);

            return $this->redirectToRoute('antibiotique_show', array('id' => $antibiotique->getId()));
        }

        return $this->render('antibiotique/new.html.twig', array(
            'antibiotique' => $antibiotique,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a antibiotique entity.
     *
     * @Route("/{id}", name="antibiotique_show")
     * @Method("GET")
     */
    public function showAction(Antibiotique $antibiotique)
    {
        $deleteForm = $this->createDeleteForm($antibiotique);

        return $this->render('antibiotique/show.html.twig', array(
            'antibiotique' => $antibiotique,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing antibiotique entity.
     *
     * @Route("/{id}/edit", name="antibiotique_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Antibiotique $antibiotique)
    {
        $deleteForm = $this->createDeleteForm($antibiotique);
        $editForm = $this->createForm('AppBundle\Form\AntibiotiqueType', $antibiotique);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('antibiotique_edit', array('id' => $antibiotique->getId()));
        }

        return $this->render('antibiotique/edit.html.twig', array(
            'antibiotique' => $antibiotique,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a antibiotique entity.
     *
     * @Route("/{id}", name="antibiotique_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Antibiotique $antibiotique)
    {
        $form = $this->createDeleteForm($antibiotique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($antibiotique);
            $em->flush($antibiotique);
        }

        return $this->redirectToRoute('antibiotique_index');
    }

    /**
     * Creates a form to delete a antibiotique entity.
     *
     * @param Antibiotique $antibiotique The antibiotique entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Antibiotique $antibiotique)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('antibiotique_delete', array('id' => $antibiotique->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
