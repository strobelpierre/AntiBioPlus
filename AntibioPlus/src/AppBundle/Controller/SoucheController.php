<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Souche;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;use Symfony\Component\HttpFoundation\Request;

/**
 * Souche controller.
 *
 * @Route("souche")
 */
class SoucheController extends Controller
{
    /**
     * Lists all souche entities.
     *
     * @Route("/", name="souche_index")
     * @Method("GET")
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $souches = $em->getRepository('AppBundle:Souche')->findAll();

        return $this->render('souche/index.html.twig', array(
            'souches' => $souches,
        ));
    }

    /**
     * Creates a new souche entity.
     *
     * @Route("/new", name="souche_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $souche = new Souche();
        $form = $this->createForm('AppBundle\Form\SoucheType', $souche);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($souche);
            $em->flush($souche);

            return $this->redirectToRoute('souche_show', array('id' => $souche->getId()));
        }

        return $this->render('souche/new.html.twig', array(
            'souche' => $souche,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a souche entity.
     *
     * @Route("/{id}", name="souche_show")
     * @Method("GET")
     */
    public function showAction(Souche $souche)
    {
        $deleteForm = $this->createDeleteForm($souche);

        return $this->render('souche/show.html.twig', array(
            'souche' => $souche,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing souche entity.
     *
     * @Route("/{id}/edit", name="souche_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Souche $souche)
    {
        $deleteForm = $this->createDeleteForm($souche);
        $editForm = $this->createForm('AppBundle\Form\SoucheType', $souche);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('souche_edit', array('id' => $souche->getId()));
        }

        return $this->render('souche/edit.html.twig', array(
            'souche' => $souche,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a souche entity.
     *
     * @Route("/{id}", name="souche_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Souche $souche)
    {
        $form = $this->createDeleteForm($souche);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($souche);
            $em->flush($souche);
        }

        return $this->redirectToRoute('souche_index');
    }

    /**
     * Creates a form to delete a souche entity.
     *
     * @param Souche $souche The souche entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Souche $souche)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('souche_delete', array('id' => $souche->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
