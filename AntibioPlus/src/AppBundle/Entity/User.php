<?php
// src/AppBundle/Entity/User.php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use FOS\UserBundle\Model\User as BaseUser;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Equipe",mappedBy="users",cascade={"persist"})
     */
    private $equipes;
    /**
     * One User has Many Etude.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Etude", mappedBy="administrateur",)
     */
    private $etudes;

    /**
     * Add equipe
     *
     * @param \AppBundle\Entity\Equipe $equipe
     *
     * @return User
     */
    public function addEquipe(\AppBundle\Entity\Equipe $equipe)
    {
        $this->equipe[] = $equipe;

        return $this;
    }

    /**
     * Remove equipe
     *
     * @param \AppBundle\Entity\Equipe $equipe
     */
    public function removeEquipe(\AppBundle\Entity\Equipe $equipe)
    {
        $this->equipe->removeElement($equipe);
    }

    /**
     * Get equipe
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipe()
    {
        return $this->equipe;
    }



    /**
     * Add etude
     *
     * @param \AppBundle\Entity\Etude $etude
     *
     * @return User
     */
    public function addEtude(\AppBundle\Entity\Etude $etude)
    {
        $this->etudes[] = $etude;

        return $this;
    }

    /**
     * Remove etude
     *
     * @param \AppBundle\Entity\Etude $etude
     */
    public function removeEtude(\AppBundle\Entity\Etude $etude)
    {
        $this->etudes->removeElement($etude);
    }

    /**
     * Get etudes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtudes()
    {
        return $this->etudes;
    }

    /**
     * Get equipes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipes()
    {
        return $this->equipes;
    }
}
