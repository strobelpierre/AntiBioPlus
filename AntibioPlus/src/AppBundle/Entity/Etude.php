<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Etude
 *
 * @ORM\Table(name="etude")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\EtudeRepository")
 */
class Etude
{
    public function __construct(){
        $this->molecule_antibioplus = new ArrayCollection();
        $this->antibiotiques = new ArrayCollection();
        $this->souches = new ArrayCollection();
        $this->equipes = new ArrayCollection();
    }
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_Debut", type="datetime")
     */
    private $dateDebut;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date_Fin", type="datetime", nullable=true)
     */
    private $dateFin;
    /**
     * Many Equipes have Many Team.
     * @ORM\ManyToMany(targetEntity="Equipe")
     * @ORM\JoinTable(name="equipe_etude")
     */
    private $equipes;
    /**
     * Many Etudes have One User.
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="etudes")
     * @@ORM\JoinColumn(name="administrateur", referencedColumnName="id")
     */
    private $administrateur;


    /**
     * Many Users have Many Groups.
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Antibiotique", inversedBy="etudes")
     * @ORM\JoinTable(name="etudes_antibiotiques")
     */
    private $antibiotiques;


    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Souche", cascade={"persist"})
     */
    private $souches;
    /**
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Molecule_AntiBioPlus", cascade={"persist"})
     */
    private $molecule_antibioplus;

    /**
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Session", mappedBy="etude")
     */
    private $sessions;

    /**
     * Get id
     *
     * @return int
     */


    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Etude
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set dateDebut
     *
     * @param \DateTime $dateDebut
     *
     * @return Etude
     */
    public function setDateDebut($dateDebut)
    {
        $this->dateDebut = $dateDebut;

        return $this;
    }

    /**
     * Get dateDebut
     *
     * @return \DateTime
     */
    public function getDateDebut()
    {
        return $this->dateDebut;
    }

    /**
     * Set dateFin
     *
     * @param \DateTime $dateFin
     *
     * @return Etude
     */
    public function setDateFin($dateFin)
    {
        $this->dateFin = $dateFin;

        return $this;
    }

    /**
     * Get dateFin
     *
     * @return \DateTime
     */
    public function getDateFin()
    {
        return $this->dateFin;
    }
    /**
     * Constructor
     */


    /**
     * Add equipe
     *
     * @param \AppBundle\Entity\Equipe $equipe
     *
     * @return Etude
     */
    public function addEquipe(\AppBundle\Entity\Equipe $equipe)
    {
        $this->equipes[] = $equipe;

        return $this;
    }

    /**
     * Remove equipe
     *
     * @param \AppBundle\Entity\Equipe $equipe
     */
    public function removeEquipe(\AppBundle\Entity\Equipe $equipe)
    {
        $this->equipes->removeElement($equipe);
    }

    /**
     * Get equipes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipes()
    {
        return $this->equipes;
    }



    /**
     * Set administrateur
     *
     * @param \AppBundle\Entity\User $administrateur
     *
     * @return Etude
     */
    public function setAdministrateur(\AppBundle\Entity\User $administrateur = null)
    {
        $this->administrateur = $administrateur;

        return $this;
    }

    /**
     * Get administrateur
     *
     * @return \AppBundle\Entity\User
     */
    public function getAdministrateur()
    {
        return $this->administrateur;
    }

    

    /**
     * Add antibiotique
     *
     * @param \AppBundle\Entity\Antibiotique $antibiotique
     *
     * @return Etude
     */
    public function addAntibiotique(\AppBundle\Entity\Antibiotique $antibiotique)
    {
        $this->antibiotiques[] = $antibiotique;

        return $this;
    }

    /**
     * Remove antibiotique
     *
     * @param \AppBundle\Entity\Antibiotique $antibiotique
     */
    public function removeAntibiotique(\AppBundle\Entity\Antibiotique $antibiotique)
    {
        $this->antibiotiques->removeElement($antibiotique);
    }

    /**
     * Get antibiotiques
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAntibiotiques()
    {
        return $this->antibiotiques;
    }

    /**
     * Add souche
     *
     * @param \AppBundle\Entity\Souche $souche
     *
     * @return Etude
     */
    public function addSouch(\AppBundle\Entity\Souche $souche)
    {
        $this->souches[] = $souche;

        return $this;
    }

    /**
     * Remove souche
     *
     * @param \AppBundle\Entity\Souche $souche
     */
    public function removeSouch(\AppBundle\Entity\Souche $souche)
    {
        $this->souches->removeElement($souche);
    }

    /**
     * Get souches
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSouches()
    {
        return $this->souches;
    }

    /**
     * Add moleculeAntibioplus
     *
     * @param \AppBundle\Entity\Molecule_AntiBioPlus $moleculeAntibioplus
     *
     * @return Etude
     */
    public function addMoleculeAntibioplus(\AppBundle\Entity\Molecule_AntiBioPlus $moleculeAntibioplus)
    {
        $this->molecule_antibioplus[] = $moleculeAntibioplus;

        return $this;
    }

    /**
     * Remove moleculeAntibioplus
     *
     * @param \AppBundle\Entity\Molecule_AntiBioPlus $moleculeAntibioplus
     */
    public function removeMoleculeAntibioplus(\AppBundle\Entity\Molecule_AntiBioPlus $moleculeAntibioplus)
    {
        $this->molecule_antibioplus->removeElement($moleculeAntibioplus);
    }

    /**
     * Get moleculeAntibioplus
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMoleculeAntibioplus()
    {
        return $this->molecule_antibioplus;
    }

    /**
     * Add session
     *
     * @param \AppBundle\Entity\Session $session
     *
     * @return Etude
     */
    public function addSession(\AppBundle\Entity\Session $session)
    {
        $this->sessions[] = $session;

        return $this;
    }

    /**
     * Remove session
     *
     * @param \AppBundle\Entity\Session $session
     */
    public function removeSession(\AppBundle\Entity\Session $session)
    {
        $this->sessions->removeElement($session);
    }

    /**
     * Get sessions
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getSessions()
    {
        return $this->sessions;
    }
}
