<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Session
 *
 * @ORM\Table(name="session")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SessionRepository")
 */
class Session
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;
    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Equipe", inversedBy="sessions")
     * @ORM\JoinColumn(name="equipeid", referencedColumnName="id")
     */
    private $equipe;

    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Test", mappedBy="session")
     */
    private $tests;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Etude", inversedBy="sessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $etude;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Antibiotique", inversedBy="sessions")
     * @ORM\JoinColumn(nullable=false)
     */
    private $antibiotique;
    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Session
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set equipe
     *
     * @param \AppBundle\Entity\Equipe $equipe
     *
     * @return Session
     */
    public function setEquipe(\AppBundle\Entity\Equipe $equipe = null)
    {
        $this->equipe = $equipe;

        return $this;
    }

    /**
     * Get equipe
     *
     * @return \AppBundle\Entity\Equipe
     */
    public function getEquipe()
    {
        return $this->equipe;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tests = new \Doctrine\Common\Collections\ArrayCollection();
        $this->equipe = new ArrayCollection();
    }

    /**
     * Add test
     *
     * @param \AppBundle\Entity\Test $test
     *
     * @return Session
     */
    public function addTest(\AppBundle\Entity\Test $test)
    {
        $this->tests[] = $test;

        return $this;
    }

    /**
     * Remove test
     *
     * @param \AppBundle\Entity\Test $test
     */
    public function removeTest(\AppBundle\Entity\Test $test)
    {
        $this->tests->removeElement($test);
    }

    /**
     * Get tests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTests()
    {
        return $this->tests;
    }

    /**
     * Set etude
     *
     * @param \AppBundle\Entity\Etude $etude
     *
     * @return Session
     */
    public function setEtude(\AppBundle\Entity\Etude $etude)
    {
        $this->etude = $etude;

        return $this;
    }

    /**
     * Get etude
     *
     * @return \AppBundle\Entity\Etude
     */
    public function getEtude()
    {
        return $this->etude;
    }

    /**
     * Set antibiotique
     *
     * @param \AppBundle\Entity\Antibiotique $antibiotique
     *
     * @return Session
     */
    public function setAntibiotique(\AppBundle\Entity\Antibiotique $antibiotique)
    {
        $this->antibiotique = $antibiotique;

        return $this;
    }

    /**
     * Get antibiotique
     *
     * @return \AppBundle\Entity\Antibiotique
     */
    public function getAntibiotique()
    {
        return $this->antibiotique;
    }
}
