<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Souche
 *
 * @ORM\Table(name="souche")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SoucheRepository")
 */
class Souche
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;
    /**
     * One Product has Many Features.
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Test", mappedBy="souche")
     */
    private $tests;
    /**
     * Many Antibiotique have Many Etude.
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Etude", mappedBy="souches")
     */
    private $etudes;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Souche
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tests = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add test
     *
     * @param \AppBundle\Entity\Test $test
     *
     * @return Souche
     */
    public function addTest(\AppBundle\Entity\Test $test)
    {
        $this->tests[] = $test;

        return $this;
    }

    /**
     * Remove test
     *
     * @param \AppBundle\Entity\Test $test
     */
    public function removeTest(\AppBundle\Entity\Test $test)
    {
        $this->tests->removeElement($test);
    }

    /**
     * Get tests
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getTests()
    {
        return $this->tests;
    }

    /**
     * Add etude
     *
     * @param \AppBundle\Entity\Etude $etude
     *
     * @return Souche
     */
    public function addEtude(\AppBundle\Entity\Etude $etude)
    {
        $this->etudes[] = $etude;

        return $this;
    }

    /**
     * Remove etude
     *
     * @param \AppBundle\Entity\Etude $etude
     */
    public function removeEtude(\AppBundle\Entity\Etude $etude)
    {
        $this->etudes->removeElement($etude);
    }

    /**
     * Get etudes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEtudes()
    {
        return $this->etudes;
    }
}
