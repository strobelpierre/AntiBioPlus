<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Test
 *
 * @ORM\Table(name="test")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TestRepository")
 */
class Test
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="Nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="Date", type="datetime")
     */
    private $date;
    /**
     * @var int
     * @ORM\Column(name="resultat", type="decimal");
     * @Assert\Range(
     *      min = 0,
     *      max = 30,
     *      minMessage = "Vous devez entrer un nombre égale ou plus grand que  {{ limit }} mm",
     *      maxMessage = "Vous ne devez pas entrer un nombre plus grand que  {{ limit }} mm"
     * )
     */
    private $resultat;
    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Session", inversedBy="tests")
     * @ORM\JoinColumn(name="session_id", referencedColumnName="id")
     */
    private $session;
    /**
     * @ORM\ManyToOne(targetEntity="Antibiotique",cascade={"persist"})
     */
    private $antibiotique;
    /**
     * @ORM\ManyToOne(targetEntity="Molecule_AntiBioPlus",cascade={"persist"})
     */
    private $molecule_antibioplus;
    /**
     * Many Features have One Product.
     * @ORM\ManyToOne(targetEntity="Souche", inversedBy="tests")
     * @ORM\JoinColumn(name="test_id", referencedColumnName="id")
     */
    private $souche;

    /**
     * Get id
     *
     * @return int
     */

    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nom
     *
     * @param string $nom
     *
     * @return Test
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Get nom
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Set date
     *
     * @param \DateTime $date
     *
     * @return Test
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Get date
     *
     * @return \DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Set resultat
     *
     * @param string $resultat
     *
     * @return Test
     */
    public function setResultat($resultat)
    {
        $this->resultat = $resultat;

        return $this;
    }

    /**
     * Get resultat
     *
     * @return string
     */
    public function getResultat()
    {
        return $this->resultat;
    }

    /**
     * Set session
     *
     * @param \AppBundle\Entity\Session $session
     *
     * @return Test
     */
    public function setSession(\AppBundle\Entity\Session $session = null)
    {
        $this->session = $session;

        return $this;
    }

    /**
     * Get session
     *
     * @return \AppBundle\Entity\Session
     */
    public function getSession()
    {
        return $this->session;
    }

    /**
     * Set antibiotique
     *
     * @param \AppBundle\Entity\Antibiotique $antibiotique
     *
     * @return Test
     */
    public function setAntibiotique(\AppBundle\Entity\Antibiotique $antibiotique = null)
    {
        $this->antibiotique = $antibiotique;

        return $this;
    }

    /**
     * Get antibiotique
     *
     * @return \AppBundle\Entity\Antibiotique
     */
    public function getAntibiotique()
    {
        return $this->antibiotique;
    }

    /**
     * Set moleculeAntibioplus
     *
     * @param \AppBundle\Entity\Molecule_AntiBioPlus $moleculeAntibioplus
     *
     * @return Test
     */
    public function setMoleculeAntibioplus(\AppBundle\Entity\Molecule_AntiBioPlus $moleculeAntibioplus = null)
    {
        $this->molecule_antibioplus = $moleculeAntibioplus;

        return $this;
    }

    /**
     * Get moleculeAntibioplus
     *
     * @return \AppBundle\Entity\Molecule_AntiBioPlus
     */
    public function getMoleculeAntibioplus()
    {
        return $this->molecule_antibioplus;
    }
   



    /**
     * Set souche
     *
     * @param \AppBundle\Entity\Souche $souche
     *
     * @return Test
     */
    public function setSouche(\AppBundle\Entity\Souche $souche = null)
    {
        $this->souche = $souche;

        return $this;
    }

    /**
     * Get souche
     *
     * @return \AppBundle\Entity\Souche
     */
    public function getSouche()
    {
        return $this->souche;
    }
}
