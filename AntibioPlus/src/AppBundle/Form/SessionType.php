<?php

namespace AppBundle\Form;

use AppBundle\Entity\Session;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SessionType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $z = new Session();
        $z = $builder->getData();

        $builder->add('nom')->add('equipe',EntityType::class,[
            'class'=>'AppBundle\Entity\Equipe',
            'choice_label'=>'Nom',
            'mapped' => true])
            ->add('antibiotique',EntityType::class,[
            'class'=>'AppBundle\Entity\Antibiotique',
            'choice_label'=>'Nom',
            'query_builder' => function(EntityRepository $er) use ($z){
             return $er->createQueryBuilder('u')
                 ->select("u")
                 ->join("u.etudes","e")
                 ->where("e.id = :id")
                 ->setParameter("id", $z->getEtude()->getId());
            },
            'mapped' => true]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Session'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_session';
    }



}
