<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use AppBundle\Entity\Session;
class TestType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $z = new Session();
        $z = $builder->getData();
        $builder->add('nom')->add('date',DateTimeType::class, array(
            'input'  => 'datetime',
            'date_widget' => 'single_text',
            'time_widget' => 'single_text',
            'label' => 'Date du test',
            'mapped' => true))
            ->add('resultat')


            ->add('molecule_antibioplus',EntityType::class,[
        'class'=>'AppBundle\Entity\Molecule_AntiBioPlus',
        'choice_label'=>'Nom',
                'query_builder' => function(EntityRepository $er) use ($z){
                    return $er->createQueryBuilder('u')
                        ->select("u")
                        ->join("u.etudes","e")
                        ->where("e.id = :id")
                        ->setParameter("id", $z->getSession()->getEtude()->getId());
                },
        'expanded'=>'false'])
            ->add('souche',EntityType::class,[
                'class'=>'AppBundle\Entity\Souche',
                'choice_label'=>'Nom',
                'query_builder' => function(EntityRepository $er) use ($z){
                    return $er->createQueryBuilder('u')
                        ->select("u")
                        ->join("u.etudes","e")
                        ->where("e.id = :id")
                        ->setParameter("id", $z->getSession()->getEtude()->getId());
                },
                'multiple'=>false,
                'expanded'=>true])      ;
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Test'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_test';
    }


}
