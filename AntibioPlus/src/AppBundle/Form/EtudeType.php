<?php

namespace AppBundle\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;

use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Doctrine\Common\Collections\ArrayCollection;

class EtudeType extends AbstractType
{
    /**
     * {@inheritdoc}
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {

        $builder->add('nom',TextType::class,array())
            ->add('dateDebut',DateTimeType::class, array(
                'input'  => 'datetime',
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
                'label' => 'Date de début',
                'mapped' => true))
            ->add('dateFin',DateTimeType::class, array(
                'input'  => 'datetime',
                'date_widget' => 'single_text',
                'time_widget' => 'single_text',
                'label' => 'Date  de fin',
                'mapped' => true))
            ->add('equipes',EntityType::class,[
                'class'=>'AppBundle\Entity\Equipe',
                'choice_label'=>'Nom',
                'multiple'=>'true',
                'expanded'=>'true',
                'mapped' => true
            ])
            ->add('administrateur',EntityType::class,[
                'class'=>'AppBundle\Entity\User',
                'choice_label'=>'username',


            ])
           ->add('antibiotiques',EntityType::class,[
               'class'=>'AppBundle\Entity\Antibiotique',
               'choice_label'=>'Nom',
               'multiple'=>'true',

               'expanded'=>'true',
               'mapped' => true
           ])
            ->add('souches',EntityType::class,[
                'class'=>'AppBundle\Entity\Souche',
                'choice_label'=>'Nom',
                'multiple'=>'true',
                'expanded'=>'true',
                'mapped' => true,
            ])
            ->add('molecule_antibioplus',EntityType::class,[
                'class'=>'AppBundle:Molecule_AntiBioPlus',
                'choice_label'=>'Nom',
                'expanded'=>'true',
                'multiple'=>'true',


    ]);
    }
    
    /**
     * {@inheritdoc}
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'AppBundle\Entity\Etude'
        ));
    }

    /**
     * {@inheritdoc}
     */
    public function getBlockPrefix()
    {
        return 'appbundle_etude';
    }


}
